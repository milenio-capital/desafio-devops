# Desafio DevOps

Bem vindo(a)! Esse desafio tem como objetivo avaliar a capacidade do candidato de estruturar um pipeline simples de integração e entrega contínuas de uma aplicação.

## O Desafio
Deverá ser desenvolvimento um pipeline de integração e entrega contínua da aplicação neste projeto. O pipeline deverá conter a criação da imagem da API, execução dos testes, execução do formatadores de código black e flake8, publicação da imagem para o ECR e publicação para o ECS. A aplicação deverá ser executada pelo ECS com Fargate e está visível para toda a internet. Toda a infraestrutura necessária deverá estar em código, com o uso do Terraform.

## Aplicação
A API foi escrita em Python com a biblioteca [FastAPI](https://fastapi.tiangolo.com/) e está conteinerizada. A seguir, os comandos necessários para o pipeline:

- Criação a imagem e execução do container:
```bash
    docker build -t devops_app_image .
    docker run --name devops_app -p 80:80 devops_app_image
```

- Execução dos testes e dos formatadores de código:
```bash
    pytest app/test.py
    flake8 app/main.py
    black app/main.py
```

## Entregáveis
- [Gitlab CI/CD](https://docs.gitlab.com/ee/ci/pipelines/) com:
	- Criação da imagem da API.
	- Execução dos testes.
	- Execução dos formatadores de código [black](https://github.com/psf/black) e [flake8](https://flake8.pycqa.org/en/latest/).
	- Publicação para o [ECR](https://aws.amazon.com/pt/ecr/).
	- Publicação para o [ECS](https://aws.amazon.com/pt/ecs).
- Arquivos do [Terraform](https://terraform.io/) com:
	- [VPC](https://aws.amazon.com/pt/vpc/).
	- [ECS](https://aws.amazon.com/pt/ecs) com [Fargate](https://aws.amazon.com/pt/fargate)
	- O que mais for necessário.
- Documentação

## Considerações
- Você deverá criar um fork privado do projeto e compartilhar conosco. Por favor, **não** abra um merge request público.
- O desafio deverá ser entregue em até uma semana. Se você não conseguir fazer tudo, não deixe de entregar. Se você empacar em alguma parte, nos procure e explique o que está acontecendo.
- Faça commits atômicos e semânticos.
